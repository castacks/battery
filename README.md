# Battery
Node to read the voltage and current readings from an ADS1x15 and publish them.
Supports either one or two independent readings.

## Usage
To run this node in dual battery mode simply set the environment variable ```SYSTEM_BATTERY_INFO``` to ```DUAL``` before running the node.

## Installation
Clone into your workspace and build

Main repo: doe_ws